# Web Interface

## Quick start

This tutorial introduces you to the essentials of Ryax usage through web interface by creating a detection workflow. The goal of this workflow is to process a video to detect object displayed on the video.

*Note :Functions used in this tutorial are not real Ryax functions.*

## Create a workflow
<video width="100%" height="auto" controls>
  <source src="_static/web_interface/create_workflow.mov" type="video/mp4">
</video>

Firstly, we need to create a new workflow. We can do this by clicking on the "plus" button located in the right side of the page header. A modal window appears to set the workflow's name. Validate by clicking on "ok" button. We're redirected to the workflow editor.
<br/>

## Adding functions to workflow
<video width="100%" height="auto" controls>
  <source src="_static/web_interface/add_functions.mov" type="video/mp4">
</video>

In this step, we will add functions used to make our video object detection workflow. First, we add the "Video Form Gateway" module used to submit a video. Then, we add the "Video Splitter" module to split video into frames. We add the "Image Object Detection" to detect objects on images. Finnaly, we add the "Video Composer" module to compose video from images.
<br/>

## Link functions
<video width="100%" height="auto" controls>
  <source src="_static/web_interface/link_functions.mov" type="video/mp4">
</video>

In this step, we will link modules together in order to use outputs from the previous module as inputs of the next module.
<br/>

## Configure functions
<video width="100%" height="auto" controls>
  <source src="_static/web_interface/configure_functions.mov" type="video/mp4">
</video>

After connecting modules together, we need to configure each of them. First, we configure "Video Splitter" module inputs to take video from "Video Form Gateway" module output. Next, we configure "Image Object Detection" module inputs to use video frames from "Video Splitter" module output. And finally, we configure "Video Composer" module inputs to use processed video frames from "Image Object Detection" module output.
<br/> 

## Deploy workflow
<video width="100%" height="auto" controls>
  <source src="_static/web_interface/deploy_workflow.mov" type="video/mp4">
</video>

Finnally, our workflow is designed and each module is configured. To use it, we have to deploy it. This action can be done by clicking on the "deploy workflow" action located in the "play" menu of the page header.
<br/>
