
# Assembling new workflows

Now that
you know how to operate an existing workflow, let's create your own!
This section will focus on workflow assembly and configuration (all
modules being already built and available in the Ryax store).


As an example, we will create a workflow called "Analyze Twitter data".
This is a more advanced version of the workflow you handled in the previous section: this improved workflow will be able to automatically fetch real Tweets, analyze them and publish them to an external service.

![](media_assembling/26a6f50b-9516-45e0-a20f-c884427b7ab1.png)

Here is a step-by-step description of this workflow:
- Trigger an execution every so often (defined by the user) with the “Emit
    Every" source module
- Pull a user defined amount of Tweets and Tweet metadata off of the twitter API according to some query, organize them and store them.
- Take the data from the Tweets and use an off-the-shelf DistilBERT model (fine-tuned for text classification) to evaluate the sentiment in those segments of text. Then organize this data and pass it along.
- \[Optional\] Take the scored data and publish it all in a structured manner to a Google Sheet.


*For more information about this workflow, please refer to its complete [documentation](../howto/text_classification_tutorial).*

To start with, let's create a new workflow from the Studio list page by
clicking on "New workflow" at the top right of the screen:


<span style="width: 400px; display: inline-block;">![](media_assembling/3b78c3dc-85d3-4d06-a17a-0e447cb22048.png)</span>


For
example, we'll name this new workflow "My own Twitter analysis". You can
also enter a description if you wish (optional).

![](media_assembling/15.jpg)

Click "Save", and the Studio Editor screen will automatically appear, you are now ready to start assembling your workflow!

![](media_assembling/16.jpg)



``` note:: To access your workflow later on, scroll or search the Studio list.
```



### Editing in the studio


#### Searching for modules

Let's start the editing process.


First
things first, you'll have to find the right modules for what you want to
do with your workflow. You can use the "Module store" panel on the right
(click the "+" button to open it) to browse through existing modules in
the store:


<span
style="width: 250px; height: 404.208px; display: inline-block;">![](media_assembling/17.jpg)</span>



The
Module Store panel displays basic information about available modules :
name, short description, type, and version.


As
a reminder, here are the different module types in Ryax (see the "Ryax
concepts" section of this guide for more information):


<table class="zw-table" data-node-id="node_0728507534570821" style="width: 100%; margin-left: 0in; table-layout: fixed; border-collapse: collapse;">
<colgroup>
<col style="width: 22%" />
<col style="width: 77%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">

![](media_assembling/af1d8202-ae6b-4de8-90f7-48bca6d53d3a.png)

</div></td>
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">
<p><span style=" font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: underline; letter-spacing: 0pt; font-weight: 700; color: rgb(0, 0, 0); vertical-align: baseline; font-style: normal; ">Sources:</span><span style="  "> to ingest data from external resources (like a database, a clock, a platform, …), also acting as workflow triggers. Often used upstream in a workflow.</span></p>
</div></td>
</tr>
<tr class="even">
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">

![](media_assembling/56bc49ea-0a0e-45d7-b5bb-b23966f926e5.png)

</div></td>
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">
<p><span style=" font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: underline; letter-spacing: 0pt; font-weight: 700; color: rgb(0, 0, 0); vertical-align: baseline; font-style: normal; ">Processors:</span><span style="  "> to process and transform data. Often used as intermediate steps in a workflow.</span></p>
</div></td>
</tr>
<tr class="odd">
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">

![](media_assembling/8d540b87-d661-4e41-97e4-4f3891ded825.png)

</div></td>
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">
<p><span style=" font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: underline; letter-spacing: 0pt; font-weight: 700; color: rgb(0, 0, 0); vertical-align: baseline; font-style: normal; ">Publishers:</span><span style="  "> to publish data to external services. Often used downstream in a workflow.</span></p>
</div></td>
</tr>
</tbody>
</table>



If
you wish to get details about a module, you can click the "?" button to
access its full page within the Ryax store:

<span
style="overflow: hidden; width: 200px; display: inline-block;">![](media_assembling/1382f5ff-8c66-491f-8816-4b5312b9590b.png)</span>


*For more information about the Ryax store and modules' documentation, see the [Ryax tour](../tutorials/handling) section.*


To
find relevant modules within the store, you can use the Quick Search
field on the right. Let's search for our first module, the "Emit every"
timer module:



<span
style="width: 250px; height: 402.367px; display: inline-block;">![](media_assembling/e5114dac-d2b8-408f-8ac9-de089917e272.png)</span>


This
module will be the Source of our workflow, and will trigger an execution
every so often.


#### Assembling workflows


To start assembling our workflow, let's drag our Source module in the canvas:

![](media_assembling/a0ab23a3-b4ab-44d9-b854-bcdc779ef832.png)

We'll proceed the same way for the other modules! We can successively fetch and drag the following modules to create the backbone of our workflow:
- "Emit every"
- "Transform Tweets"
- "Text Classifier (distilBERT)"

In the end, your Editor's canvas should look something like this:

![](media_assembling/fbf25d45-c1a7-43ef-a001-0401f9c65ffa.png)

At this stage, we've gathered all workflow's necessary modules in the canvas.
For our workflow to become a true treatment chain, let's link the modules together in the desired order.


For this, just hold and drag the Link Connector control from one module to the next:


<span
style="width: 600px; display: inline-block;">![](media_assembling/986d15fe-9be2-431f-9c41-188c8e6ce633.png)</span>



Repeat
this linking process for the rest of the workflow and you'll get a fully
interlinked treatment chain:


<span
style="width: 600px; display: inline-block;">![](media_assembling/be095685-31b9-4945-8830-ba18a976659d.png)</span>



#### Assessing studio statuses


At this
stage you can note that modules still display a "!" on their top right
corner, which are warnings of potential incomplete configuration or
interlinking. Ryax natively provides such warnings to help you only
deploy ready-to-production workflows.

Additionally,
our workflow's general status itself is "Incomplete", which means it is
not yet ready for deployment due to improper config of its embedded
modules:


<span
style="width: 90px; height: 30.974px; display: inline-block;">![](media_assembling/49b8a5af-257b-4c8b-90df-5f7d9432ee3e.png)</span>



To get more details about these warnings, let's click the "Warning" panel button on the right of the Editor:

<span
style="width: 350px; display: inline-block;">![](media_assembling/720058a6-cdc1-4c45-8bbb-4dfb6bb04fa7.png)</span>



Using the
module warnings panel, you'll be able to assess what could prevent your
workflow from effectively running in production. In this example, all
the warnings come from incomplete configurations of our modules, which
is leading us to the next section: configuring modules.


#### Configuring modules


Our
workflow is fully assembled and ready to be configured. As you can see,
the "!" status displays indicate that some operations are still needed
before our modules are in a ready state for deployment.


##### Source "Emit every" module


Let's select the first "Emit every" module in the workflow, the contextual "Module setup" panel will automatically open on the right.

From this
configuration panel will be able to quickly setup any module for
production. Navigate to the Inputs tab and set it up as follows:


<span
style="width: 300px; display: inline-block;">![](media_assembling/5c4d857b-291f-43cd-ba68-193f18cf0ac3.png)</span>&nbsp;&nbsp;&nbsp;&nbsp;<span
style="width: 300px; display: inline-block;">![](media_assembling/9375823e-5f20-4d8f-bdea-618cee726b4e.png)</span>


This
module is a timely trigger, which means it can generate executions with
a fixed frequency. Entering "1d" in the input field implies
that our
Twitter analysis workflow will be triggered every other day.


No action
is required on the two remaining tabs since they are already set by
default. If you want, you can change the "Custom name" of the module
from the "Properties" tab for more clarity when displayed in the
workflow:


<span
style="width: 300px; display: inline-block;">![](media_assembling/8612d9aa-2485-47b8-9929-7a171d9960d3.png)</span>

<span
style="width: 300px; display: inline-block;">![](media_assembling/329787ae-e700-44f4-927e-7e31af198dc3.png)</span>



##### Processor "Get from Twitter" module

Moving on
to the next module. As this module handles data retrieval from
Twitter's
developer API, we will have to specify a query for the search. No need to provide credentials, on this one we will use our internal credentials hidden away, so you can simply go ahead and try it out.


<span
style="width: 300px; display: inline-block;">![](media_assembling/twitter_in.png)</span>

Configuration is simple:
-   <span
    style="text-decoration: underline;">Query:</span> this is a string which we use to search for tweets. If a hashtag, or a hashtag plus some language is desired, just use a \# in the query.
    --&gt; As an example you can use <span
    style=" font-weight: 700;">\#DataScience</span>

Here is how the module looks after setup:

<span
style="width: 300px; display: inline-block;">![](media_assembling/twitter_in_after_setup.png)</span>


No further
configuration is required for this module, and it will display a "green
check" status upon completion. When run, the module will fetch 50 tweets searched for according to the input query, and will save those tweets along with some metadata such as timestamps, and usernames, in a tabular fashion in an encoded file. The module returns that file.


##### Processor "Text classifier" module


This
is a typical module that a Data Scientist could have developed. It will
take a series of Tweets as an input, and generate a classification as an
output. Little configuration is required for this module since it is
built to analyze speech and evaluate its overall
positiveness/negativeness.


The
only thing we'll have to do is specify its input, which has to be the
previous module's input. To do so in Ryax, navigate to the "Input" tab
of the "Module Setup" pane, select the "Reference value" option next to
the input field (this will specify that you want to use a reference data
stream, already present somewhere within Ryax). Then, you can select the
data stream coming from the upstream module, "Resulting data". This will
tell Ryax that you want the current module to ingest the previous
module's data.


<span
style="width: 300px; display: inline-block;">![](media_assembling/46508bcd-5b99-49e5-a325-ba69811e8ccd.png)</span>

<span
style="width: 300px; display: inline-block;">![](media_assembling/fd0f8862-1534-4531-8e79-bdd0bb51ee5f.png)</span>


*More information about this module [here](../howto/text_classification_tutorial).*



##### \[Optional\] Publisher "Google Sheet" module

This
part is optional because it requires more advanced actions to be set up.
Using our "Google Sheet" publisher module, you'll be able to display
Tweet classification results directly in a Google Sheet hosted online,
instead of getting results from Ryax's native execution
dashboard.

![](media_assembling/ab9bdf8f-d060-4090-aee3-bf0476f1a846.png)


To configure this publisher, you'll need a Google Sheet API token as
described in the [Sentiment Analysis on tweets](../howto/text_classification_tutorial.html#publish-to-google-sheet) side-tutorial (estimated time: 10 minutes maximum).

There
you go! This module has no output and will perform a simple print of the
return message from the google API when executed.



### Deploying from the studio


As
you learned from the previous sections and exercises, within Ryax a
workflow has to be in the "deployed" state
to run in production. Deployment is the phase where the workflow is
built, packaged and loaded by Ryax on production infrastructures.


Before
deploying a workflow, we'll have to make a quick sanity check to ensure
it is ready to do so: does it display an "OK" status? If yes, then in
most cases this check is all you will need to do, provided that you've
also had a look at the "Warning" panel in the Editor and took care of
fixing all potential warning messages.


Once
in the Editor, you can click on the "Warning" panel (on the bar to the
right) to open it:

<span
style="width: 600px; height: 363px; display: inline-block;">![](media_assembling/18.jpg)</span>

This
panel displays any warning or errors Ryax has detected on your workflow.
If the "Workflow ready" message displays, in accordance to the "OK"
status of the workflow, then we're ready to proceed with the
deployment.


You
can now click any "Deploy" button, either from the Workflow list view or
from the Studio Editor:


<span
style="width: 100px; height: 45px; display: inline-block;">![](media_assembling/7181ea04-73ee-49a1-98ec-e841f182be3f.png)</span>


Upon
deployment, your workflow will display a succession of statuses:
"Creating", "Deploying", and finally "Deployed". It should take a few
seconds, if the displayed workflow status does not change, please click
the "Refresh" button at the top of the page to actualize.


As
a reminder of workflow statuses (covered in the "Ryax tour"
sections):



<table class="zw-table" data-node-id="node_06324169338822316" style="width: 100%; margin-left: 0in; table-layout: fixed; border-collapse: collapse;">
<colgroup>
<col style="width: 26%" />
<col style="width: 73%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">

![](media_assembling/7de9563a-77ef-4d18-be99-e99753deaec6.png)

</div></td>
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">
<p><span style=" font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: underline; letter-spacing: 0pt;  color: rgb(0, 0, 0); vertical-align: baseline; font-style: normal; ">OK</span><span style="  ">: workflow is properly assembled and configured, it is ready to be deployed.</span></p>
</div></td>
</tr>
<tr class="even">
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">

![](media_assembling/b90a868e-1de7-4807-b382-4be5e54255d8.png)

</div></td>
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">
<p><span style=" font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: underline; letter-spacing: 0pt;  color: rgb(0, 0, 0); vertical-align: baseline; font-style: normal; ">Incomplete</span><span style="  ">: workflow is not ready to be deployed (configuration is incomplete, assembly is incorrect, etc).</span></p>
</div></td>
</tr>
<tr class="odd">
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">

![](media_assembling/956534ba-bb91-4b32-917f-5b6cfb7cea4d.png)

</div></td>
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">
<p><span style=" font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: underline; letter-spacing: 0pt;  color: rgb(0, 0, 0); vertical-align: baseline; font-style: normal; ">Creating</span><span style="  ">: workflow deployment is initiated and the workflow is being instantiated in production.</span></p>
</div></td>
</tr>
<tr class="even">
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">

![](media_assembling/f9e47823-25c1-4d64-b21d-5ea77927b667.png)

</div></td>
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">
<p><span style=" font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: underline; letter-spacing: 0pt;  color: rgb(0, 0, 0); vertical-align: baseline; font-style: normal; ">Deploying</span><span style="  ">: workflow is being deployed.</span></p>
</div></td>
</tr>
<tr class="odd">
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">

![](media_assembling/ef2f053d-954d-47cd-bdfb-f442784ab81c.png)

</div></td>
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">
<p><span style=" font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: underline; letter-spacing: 0pt;  color: rgb(0, 0, 0); vertical-align: baseline; font-style: normal; ">DeployError</span><span style="  ">: workflow could not be deployed. Check module configuration for warnings or errors.</span></p>
</div></td>
</tr>
<tr class="even">
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">

![](media_assembling/30dccd63-45f2-4703-af16-57e4486bd299.png)

</div></td>
<td><div class="zw-cell-container" style="position: relative; padding: 13.248px;">
<p><span style=" font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: underline; letter-spacing: 0pt;  color: rgb(0, 0, 0); vertical-align: baseline; font-style: normal; ">Deployed</span><span style="  ">: workflow is ready to receive inputs and generate executions.</span></p>
</div></td>
</tr>
</tbody>
</table>


The "Deployed" status ensures that your workflow has been successfully deployed in production: congratulations!



### Checking executions

Depending on how your workflow is built, the type of events leading to workflow triggering may differ.
In the case of our "Analyze Twitter data" workflow, the triggering event will be a timer, as indicated by the
first module (i.e. "source" module) in our workflow:

<span
style="width: 150px; height: 72px; display: inline-block;">![](media_assembling/4510fad6-7d10-4adc-8d01-a69ddf093d15.png)</span>


Once
we deployed our workflow, this first module has been automatically
instantiated and is now running on its own. Since this source module is
built to emit events by itself on a regular basis, no further action is
required from the user to trigger a workflow execution: meaning that our
Twitter analysis will simply run "every X second/minute/hour...".


To get a
reminder as how it was configured, let's click its module box in the
Editor's canvas and navigate to the "Input" tab:


<span
style="width: 600px; height: 349.677px; display: inline-block;">![](media_assembling/972cf52f-5991-4bd4-ac5a-405362d96802.png)</span>


As
indicated in the input box, our workflow will run once a day. If you
wish to change this frequency, you can simply undeploy the workflow,
modify the input value and deploy the workflow again. This can be done
anytime.


Now to
executions: when we deployed our workflow the source module triggered
its first execution. Let's click the "View executions" button to check
it out:


<span
style="width: 250px; height: 26.3285px; display: inline-block;">![](media_assembling/c708bb5c-17b7-4460-b5e2-a275c981f05f.png)</span>


You'll be
redirected to your workflow's execution list:


<span
style="width: 600px; height: 367.487px; display: inline-block;">![](media_assembling/dd0d80e9-132c-4c87-ac72-a1d540b01528.png)</span>


Depending
on when you access this screen, there may still be some modules running in the "Submitted" or "Running" stage.
This is normal as some steps will take a few minutes to complete: e.g.
the Classifier module make take some time to upload and ingest hundreds
of Tweets at once.


You can
refresh the list to watch the execution propagate, after a few minutes
all modules should have executed successfully ("Done" status), as the
workflow ran from end to end:


<span
style="width: 600px; height: 357.184px; display: inline-block;">![](media_assembling/84efab0f-0a38-4d52-8f93-4cfb3c7fc255.png)</span>


To access
the final results of your workflow, click on any "Text classifier"
execution. You'll view the classification results in the log window of
the execution dashboard:


<span
style="width: 600px; height: 357.184px; display: inline-block;">![](media_assembling/eba2dd74-7ee9-447b-8980-729e5b4a4c62.png)</span>



As you can
see in this example (which will very probably be different from yours,
since the workflow pulls actual Twitter data), the first Tweet was
deemed as negative with a 98.78% confidence score, the third Tweet is
judged rather positive with a 95.47% confidence index, etc:

![](media_assembling/99d4cb51-0896-434d-98a4-e9d308418b4f.png)


#### \[Optional\] Check executions in the Google Sheet</span>


If you've
set up the optional Google Sheet API publisher module, then you'll be
able to access a more advanced result visualization of your workflow.
Simply type the Google Sheet's URL you've created in your browser to
watch the output data:

![](media_assembling/efde8499-45e5-4bd1-8bb7-cb009ab50292.png)


The sheet
displays the Tweets' timestamp, author, content and the executed
workflow results: algorithm interpretation (positive or negative), and
confidence index (between 0 and 1).

Please
note that any additional execution of the workflow will append its
results below existing rows.


