# Ryax public doc

**Home of [docs.ryax.tech](https://docs.ryax.tech/).**

## Local build

### Using Nix

In order to build and see the doc locally do:

```sh
nix-build release.nix
```

__WARNING__: API spec documentation might fail with CORS error on local
deployment. Use the hosted version instead.

If you commit your changes, they will be online a few minutes after, at https://docs.ryax.tech/ .

### Using your local python installation

```sh
make html
firefox ./result/html/index.html
```

It requires several python packages!


## Adding a new version of the API

Get the API spec either from the internal documentation
(https://ryax-tech.gitlab.io/dev/backend/ryax_api/apispec.html#downloads) or
from a local build of ryax_api using:
```
nix-build release.nix -A docs
# the spec file is now in ./result/spec.json
```

Now, import the spec into the do with:
```sh
./add_api_spec.sh ./spec.json
```

The version of the API is taken from the spec file but you can create another
version by adding a second parameter:
```sh
./add_api_spec.sh ./spec.json latest
```

## Importing document from Zoho Docs

- Export the document as html
- `pandoc output.html -o newdoc.md  -t commonmark`
- To be able to see images in tables:
    - replace: `<p><span style="margin-left: 0in; border-width: 0px; border-style: none; border-color: rgb\(0, 0, 0\); padding: 0px; border-radius: 0px; width: ([^;]+); height: ([^;]+); display: inline-block;"><img src="([^"]+)" /></span></p>`
    - with: `\n![](\3)\n`
- edit the titles, they do not appears well
- you can remove all occurences from `font-family: Carlito, Calibri; font-size: 12pt;`
- keep only images regarding one file: `mkdir media_running ; mv $(grep media/.*png running.md  -o) media_running`
- to optimize images: `for i in * ; do convert $i -resize 800\> $i ; optipng $i ; done` (resize image bigger than 800px to 800px (max size of the web page) and optimize file)



## Inspirations

- https://typer.tiangolo.com/
- https://relay.sh/blog/command-line-ux-in-2020/
- https://www.terraform.io/intro/use-cases.html
- https://stripe.com/docs : elected the best doc ever by HN users
- https://docs.djangoproject.com/en/2.0/ : often called the best API doc.
- https://redis.io/documentation : a simple good doc
- https://node-postgres.com/ : a simple good doc
- https://www.postgresql.org/docs/10/index.html ; a good doc for a complex system.
- https://documentation.divio.com/introduction/ : Seprate docs in 4: tutorials (to learn), how-to guides (for precise tasks), references (to describe the machinery), explanation (to understand).





