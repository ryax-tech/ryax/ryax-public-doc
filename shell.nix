{
  pkgs ? import <nixpkgs> { }
}:
pkgs.mkShell {
  buildInputs = with pkgs.python3Packages; [
    sphinx
    sphinx_rtd_theme
    recommonmark
  ];

  LC_ALL="en_US.UTF-8";

  shellHook = ''
    SOURCE_DATE_EPOCH=$(date '+%s');
    '';
}
