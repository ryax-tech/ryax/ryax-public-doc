
.. meta::
   :description: Ryax: create quickly data automations in the cloud.
.. meta::
   :property="og:description": Create quickly data automations in the cloud.
.. meta::
   :property="og:title": Ryax Technologies.
.. meta::
   :property="og:url": https://docs.ryax.tech/index.html
.. meta::
   :property="og:image": https://docs.ryax.tech/_static/ryax-icon.png

Ryax Documentation
==================


Ryax is an open-source platform that enables the design, deployment, and monitoring of your data analytics, Cloud automations and GenAI applications.

Ryax makes the creation and execution of GenAI and data analytics apps more accessible to all types of developers. It enables users to execute their GenAI and data analytics applications across the Edge/Cloud/HPC continuum seamlessly. It provides smart scheduling capabilities while abstracting the complexity of managing applications in production.

This is the best place to start with Ryax. In the following sections we cover what Ryax is, what problems it can solve, and how it compares to existing software.




Need a quick crash course? Jump right in our `Quick Start Guide <./tutorials/quick_start_guide.html>`_




What is Ryax?
-------------

.. image:: ./_static/web_interface/ryax-screenshot-execution.png


.. Seprate docs in 4: tutorials (to learn), how-to guides (for precise tasks), references (to describe the machinery), explanation (to understand).

.. toctree::
   :maxdepth: 2
   :caption: Concepts
   
   ./concepts/intro
   ./concepts/concepts
   ./concepts/comparisons
   ./concepts/use_cases


.. toctree::
   :maxdepth: 2
   :caption: Tutorials

   ./tutorials/quick_start_guide
   ./tutorials/multi-site-workflow
   ./reference/action_python3
   ./tutorials/action_csharp
   ./tutorials/action_nodejs
   ./tutorials/http_api_json
   ./howto/jupyter
   ./howto/text_classification_tutorial
   ./reference/hpc
   ./tutorials/reproducible_action_build.md


.. toctree::
   :maxdepth: 2
   :caption: How-to guides

   ./howto/install_ryax_kubernetes
   ./howto/worker-install
   ./howto/spark_piloted_action
   ./howto/create-backups


.. toctree::
   :maxdepth: 2
   :caption: Reference

   ./reference/action_python3
   ./reference/configuration.md
   ./reference/architecture
   ./concepts/projects
   ./reference/glossary
   ./reference/basic_actions
   ./reference/ryax_lock.md
