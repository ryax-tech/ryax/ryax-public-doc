# Release Notes

These release notes provide information about new features, bug fixes and security updates that are included in Ryax.


--------
## Ryax 21.07.1

Released the: 2021-07-15T14:04:04.414Z

### New features

- Modules can now be categorized! Add "categories" in your ryax_metadata.yaml file, then you can filter by category in the module store and the panel of the workflow studio.
- The form module now supports directories.
- Official support for binary dependencies in the modules is now available. Simply add some "dependencies" in your ryax_metadata.yaml file.
- From now on when deleting a workflow, all of its executions are removed.
- Improved messages when there is an error when scanning a repository.

### Bugfixes
- Fix: Workflow states are sometimes stuck in "deploying"
- Fix: Scanning fails when the Git repository is too big
- Several small bug fixes and performance optimizations


### Upgrade to this version
Admins should take care of the following elements when upgrading to this version:
- In `adm`, disable the authorization micro-service
- Non-durable queues and exchanges should be removed after the upgrade, otherwise some micro-services won't start. To do so, port-forward the rabbitmq interface to your local machine: `kubectl port-forward -n ryaxns svc/ryax-broker 15672:15672` (the service port might be wrongly set to 15692, in this case, edit the service). Get the user/password (`kubectl -n ryaxns get secret ryax-broker-secret -o jsonpath={.data.broker-user} | base64 --decode` and `kubectl -n ryaxns get secret ryax-broker-secret -o jsonpath={.data.broker-pass} | base64 --decode`). Login in the admin rabbitmq panel and delete all exchanges and queues that are not durable (they do not have a blue "D" in the features column). Then, manually restarts the pods that are crashing or wait for kubernetes to do it.

--------
## Ryax 21.06.7

Released the: 2021-06-01T14:36:41.930Z

### New features

- Module version is now optional: if you do not give a version to a module, Ryax will automatically detect the new version.
- Modules can be updated very easily: just change the version in the information panel! Inputs, outputs and links are kept when updating a module.
- It is now possible to pan and zoom in the workflow studio. You can now create gigantic workflows!
- A command line to interact with Ryax is released in beta version, check it here: https://docs.ryax.tech/reference/ryax_cli.html .
- Some standard Ryax modules have been open-sourced
- The logo of the platform has been updated from Ryax Tech. (the company) to Ryax (the product).
- Improve monitoring of all internal services


### Bugfixes

- Fix: A popup opens everytime that I type in a module's input
- Fix: When an exception happens in the launcher, the execution is still reported as successful
- Fix: internal module registry is not purge from old modules
- Fix: Static input files are never deleted
- Fix: Modules are never deleted from the registry which make the module build fail

--------
## Ryax 21.06.6

Released the: 2021-05-05T13:26:26.736Z

### New features
- Improve the UI of errors in module builds
- Add new modules: Gitlab and Cliq!
### Bugfixes
- In the workflow studio, the right panel was hard to open with some input devices, such as trackpads
- Modules inputs were not saved within specific circumstances
- Fix some typos

--------
## Ryax 21.06.5

Released the: 2021-04-28T12:41:38.127Z

### New features
- Upgrade the web UI to the last version of its framework, expect performance improvements!
- Add a "settings" page so that users can change their password and personnal informations
- SaaS instances are now instanciated with a set of 24 modules.
- Better handling on modules logs: Ryax now supports binary outputs and limit the size of logs to 1 MB.
- Improve forms display
### Bugfixes
- No more needs to hard refresh webpage on ryax upgrade
- Prevent modules from using the same name for multiple inputs or outputs
- On some situation, modules outputs were not shown on the store
- Prevent workflows to be stuck on the "deploying" state
- Fix bugs that considers "0" to be equal to null in modules inputs.

--------
## Ryax 21.06.4

Released the: 2021-04-14T07:49:50.798Z

### New features
- Users can retrieve why a build fails
- Hide passwords in the execution page.
- Show the number of errors in a workflow, even when the right panel is closed.
- In the "execution" page, move the "logs" card to the bottom of the page.
- Improve the way modules are displayed in all screens.
- It is now obvious that users can drag&drop files in forms.
### Bug fix
- Remove several bugs linked with ryax_metadata.yaml formatying
- Fix: an empty requirements.txt make the scan fails


--------
## Ryax 21.06.3

Released the: 2021-03-30T07:47:11.785Z

### Bug fix
- Fix file extension removing when on saving action.

--------
## Ryax 21.06.2

Released the: 2021-03-25T14:48:27.742Z

### Bug fix
- Fix repository reconnection to broker after broker restarted.

--------
## Ryax 21.06.1

Released the: 2021-03-25T14:02:21.688Z

### Bug fix
- Fix module builder reconnection to broker after broker restarted.

--------
## Ryax 21.06.0

Released the: 2021-03-22T08:55:17.392Z

### Features
- Add more informations (Path, ID, Version, etc...) on scanned modules.
- Handle static files during import/export by using zip file.
- Allow to scan public repositories without credentials (username and password).

### Bug fix
- Handling special characters in repository scan password.

--------
## Ryax 21.05.1

Released the: 2021-03-15T09:44:08.459Z

### Features

None for this release

### Bug fix
- Fix bug on user edition.

--------
## Ryax 21.05.0

Released the: 2021-03-03T16:19:07.953Z

### Features
- Handle workflow module static value for file input

### Bugfixes
- Fix execution filter by module in list view.
- Disabled workflow module link creation when workflow is deployed.
- Fix bug on user edition.
- Add link to go to previous page on unauthorized page.
- Fix outputs and inputs display in execution details page when coming from list view.

--------
## Ryax 21.04.1

Released the: 2021-02-18T15:02:59.260Z

### Features
None

### Bugfixes
- Fix bug on execution not trigger when modules is returning nothing.

--------
## Ryax 21.04.0

Released the: 2021-02-18T10:44:47.872Z

### Features
- Workflow export to yaml format.
- Workflow import from yaml format.

### Bugfixes
- Fix bug on workflow module input reference value that cleaned when a module is removed from a workflow.

--------
## Ryax 21.03.0

Released the: 2021-02-10T11:50:20.329Z

### Features
- Forbid module deletion from the workflow when it's used in a workflow.
- Possibility to see unique identifier of the module in store details screen and in studio module informations pane.
- Prevent user from building already built module, defined by its identifier and version.

### Bugfixes
- Fix bug on workflow filtering from studio list view.
- Make only sources available as workflow portals.
- Block module deletion in deployed workflow.
- Block module pane opening in deployed workflow (to prevent module addition).
- Block module link deletion in deployed workflow.
- Block module properties update in deployed workflow.
- Fix bug on workflow validity status after creation.

--------
## Ryax 21.02.1

Released the: 2021-02-02T17:08:45.067Z



--------
## Ryax 21.02.0

Released the: 2021-02-02T08:36:34.103Z

#### General
- More responsive user interface
- More than 30 bugs fixed
- CLI deprecated

#### Module Building
- Deprecated feature: adding code directly to Ryax
- Modules are now added to Ryax using Git repositories
- Faster module builds
- More checks in the pre-building phase of imported modules (check for general requirements, duplicates...)
- Module IDs are visible when scanning Git repositories

#### Module store
- Deleting a module that is currently used in a workflow triggers a warning panel to act accordingly

#### Workflow studio
- More detailed errors
- More contextual information available in the Studio right panel
- Better detection of non-supported workflows
- Workflow import/export

#### Web portals
- Improved access to results in executions panels
- Improved error reporting


--------
## Ryax 20.11.0

Released the: 2020-11-13T23:42:20.413Z

### New features

- New module Repository with module scan and import from a Git repository.
- Rework of ryax-adm with enhanced capabilities, a proper cli with autocompletion, and yaml based configuration.

#### Known bugs

TO BE UPDATED:
- [File of 1G size fails to upload](https://gitlab.com/ryax-tech/dev/backend/ryax_common/-/issues/19)
- [When we stop a workflow, running executions stay in running state](https://gitlab.com/ryax-tech/dev/backend/core/-/issues/13)
- [Builder loose message sometimes, happens often on CI tests](https://gitlab.com/ryax-tech/dev/backend/effects/builder/-/issues/7)

--------
## Ryax 20.09.0

Released the: 2020-09-22T15:41:16.541Z

### Installation

```json
{
    "version": "20.09.0",
    "registry": "registry.ryax.org",
    "environment": "production"
}
``` 

### Backend

- Better monitoring and alerting
- Remove old useless code
- Better detect non supported workflows
- Improve execution view
- improve dev documentation
- Fix empty custom names for modules
- Fix errors in the creation of protected micro-services
- Fix dynamic outputs editing problems

#### Known bugs

- [Workflow deployment must be kept if an execution refers to it](https://gitlab.com/ryax-tech/dev/backend/api/-/issues/61)
- [File of 1G size fails to upload](https://gitlab.com/ryax-tech/dev/backend/ryax_common/-/issues/19)
- [When we stop a workflow, running executions stay in running state](https://gitlab.com/ryax-tech/dev/backend/core/-/issues/13)
- [Builder loose message sometimes, happens often on CI tests](https://gitlab.com/ryax-tech/dev/backend/effects/builder/-/issues/7)

### Frontend

- Integrate forms inside Ryax WebUI
- add search form in workflow list
- add search form in module list


#### Known bugs

- [Executions list is stuck in loading for Firefox 77 and Safari](https://gitlab.com/ryax-tech/dev/ryax-webui/-/issues/571)
- [Execution details previous and next executions are blank in some case](https://gitlab.com/ryax-tech/dev/ryax-webui/-/issues/570)
- [Error 404 page is not loaded](https://gitlab.com/ryax-tech/dev/ryax-webui/-/issues/572)


--------
## Ryax 20.08.1

Released the: 2020-08-27T12:50:04.104Z

### Installation

```json
{
    "version": "20.08.1",
    "registry": "registry.ryax.org",
    "environment": "production"
}
``` 

### Backend

- Add endpoint to manage projects
- Add endpoint to manage and retrieve profiles
- Add endpoint to retrieve executions' tree
- Major docummenation improvements: ryaxpkgs, infra, ci, cli
- Associated ryax resources to project
- Associated user session with project
- Fix update workflow owner bug
- Refactor gitlab projects to cope with resource names

#### Known bugs

- [Workflow deployment must be kept if an execution refers to it](https://gitlab.com/ryax-tech/dev/backend/api/-/issues/61)
- [File of 1G size fails to upload](https://gitlab.com/ryax-tech/dev/backend/ryax_common/-/issues/19)
- [When we stop a workflow, running executions stay in running state](https://gitlab.com/ryax-tech/dev/backend/core/-/issues/13)
- [Builder loose message sometimes, happens often on CI tests](https://gitlab.com/ryax-tech/dev/backend/effects/builder/-/issues/7)

### Frontend



#### Known bugs

- [Executions list is stuck in loading for Firefox 77 and Safari](https://gitlab.com/ryax-tech/dev/ryax-webui/-/issues/571)
- [Execution details previous and next executions are blank in some case](https://gitlab.com/ryax-tech/dev/ryax-webui/-/issues/570)
- [Error 404 page is not loaded](https://gitlab.com/ryax-tech/dev/ryax-webui/-/issues/572)


--------
## Ryax 20.08.0

Released the: 2020-08-13T08:08:12.927Z

### Installation

```json
{
    "version": "20.08.0",
    "registry": "registry.ryax.org",
    "environment": "production"
}
```

### Backend

- Expose module instance inputs and outputs in the execution API
- Workflow deployment instant update though web socket
- More information from Kubernetes about deployments and pods are watched
- Preparation work for module Profiles support
- Preparation work for Project support
- Improve internal documentation (newcomers, launchers)
- Add unit tests for the API
- Compiled images are working again

#### Known bugs

- Workflow deployment must be kept if an execution refers to it (https://gitlab.com/ryax-tech/dev/backend/api/-/issues/61)
- Very large file (1G) upload fails (https://gitlab.com/ryax-tech/dev/backend/ryax_common/-/issues/19)
- When we stop a workflow, running executions stay in running state (https://gitlab.com/ryax-tech/dev/backend/core/-/issues/13)

### Frontend

- Move help button to navigation bar
- Delete "deployments" entry from the nav bar, and put Studio and Monitoring outside the same submenu
- Disable "delete workflow" button when workflow is deployed
- In Sandbox > Editor, the "Code & Dependencies" tab should contains a message if the form_id is not textinput
- Add color line/identifier to each type of modules to differentiate them on the graph
- Add button to view executions with filter based on modules
- Add button to view executions with filter based on workflow 
- Error when deleting dynamic outputs
- Fix visual bug on filter, on the monitoring page
- Enable filtering executions by modules
- Add module tool in workflow details


#### Known bugs

- Executions list is stuck in loading for Firefox 77 and Safari https://gitlab.com/ryax-tech/dev/ryax-webui/-/issues/571
- Execution details previous and next executions are blank in some case https://gitlab.com/ryax-tech/dev/ryax-webui/-/issues/570
- Error 404 page is not loaded https://gitlab.com/ryax-tech/dev/ryax-webui/-/issues/572

--------
## Ryax 20.07.3

Released the: 2020-07-29T20:16:44.661Z

### Release Notes

* Finish migration to postgresql
  * Added more options to filter on endpoint /executions
  * Paging support for endpoint /executions
  * Add support for webui: real paging
  * Fix long time fetching executions
* Added support for dynamic outputs
* Improve workflow editor UX
  * Workflow draft and studio are rendered in the same place
* Security: modules to test forkbombs and file rights' abuse
* New module: push file to GCP sotrage
* Added support for mulitple connections without re-login to cli
* Adeed "owner" fields now with id and the username to api
* Return a path to a downloadable file when retreive a file type IO
* Several improvements in documentation
* Fix bug on float inputs
* Fix building of an existing module
* Add check to avoid 2 inputs with same name

### Installation

```json
{
    "version": "20.07.3",
    "registry": "registry.ryax.org",
    "hostnames": [
        "staging.ryax.org"
    ],
    "environment": "staging"
}

--------
## Ryax 20.07.2

Released the: 2020-07-15T15:55:39.467Z

### Release Notes

* Flag workflow without modules as incomplete
* Fix bug when removing module that has incoming dependency
* Support postgreSQL as new datastore
* New source: "every" that generates a new execution <every> second. (First contribution of Charles to the backend !)
* Users can access details about others
* Security mechanism to prevent users to list all users

### Installation

```json
{
    "version": "20.07.2",
    "registry": "registry.ryax.org",
    "hostnames": [
        "staging.ryax.org"
    ],
    "environment": "staging"
}

--------
## Ryax 20.07.1

Released the: 2020-07-10T11:40:59.730Z

### Release Notes

* Clean workflow broken links when deleting modules with incoming arrows
* Set as incomplete workflows with no modules
* Fix API login error response

### Installation

```json
{
    "version": "20.07.1",
    "registry": "registry.ryax.org",
    "hostnames": [
        "staging.ryax.org"
    ],
    "environment": "staging"
}
```

--------
## Ryax 20.07.0

Released the: 2020-07-03T18:49:11.107Z

### Release Notes

- Add support link and help forms
- Better workflow status refresh and colors
- Error display improved in the studio
- New file access API endpoint
- New execution cleaning API endpoint
- New HTML Form Source Module with I/O types validation (a.k.a form gateway)
- Several Bug fixes

### Installation

```json
{
    "version": "20.07.0",
    "registry": "registry.ryax.org",
    "hostnames": [
        "staging.ryax.org"
    ],
    "environment": "staging"
}
```

--------
## Ryax 20.06-alatus-rc4

Released the: 2020-06-18T06:37:40.152Z

### Release Notes

* Add error feedback on Workflow Studio;
* Improved error feedback per function on workflow draft;
* Add incentive on Workflow Studio;
* Improved icons;
* Fix logout button;
* Several improvements to enhance the user experience.

### Installation file

```json
{
    "version": "20.06-alatus-rc4",
    "registry": "registry.ryax.org",
    "hostnames": [
        "example.ryax.tech"
    ],
    "environment": "production"
}
```

--------
## Ryax 20.06-alatus-rc3

Released the: 2020-06-09T11:58:49.642Z

### Release Notes

* Allow copy of workflows;
* Added filter for executions;
* Improve test stability and ssl support;
* Fix upload of big files;
* Fix executions' stuck in "RAN" state;
* Gateways' executions default to non null execution time;
* Avoid loosing internal messages;
* Functions can run for unlimited amount of time;
* Add function_deployment_id field to executions's Schema";
* Improve error messages when triggering not allowed action;
* Updated CLI, ADM;
* Several improvements to enhance user experience.

--------
## Ryax 0.0.6

Released the: 2020-01-23T09:06:54.305Z

### Release Notes

* Remove function running column table
* Enable password input type for function creation
* Enable directory input type for function creation 

--------
## Ryax 0.0.5

Released the: 2019-12-20T09:34:30.317Z

### Release Notes

* Overall reactivity improvement;
* Added test to verify VM demo health;
* Local file sharing, enable to fetch results no need for external network storage;
* Improve python3 function creation, syntax and format check;
* Mock function creation from container image;
* Logs of functions accessible from execution detailed view;
* VM final size reduced to 5.6 GB;
* General stability improvements to enhance the user's experience.

### Known Bugs

* Execution history only show state DONE and ERROR (missing WAITING, RUNNING);
* Delete function, on workflows that had previous executions, leads to 404 error on executions view.



