
import gitlab
import os
import re

if "GITLAB_TOKEN" not in os.environ :
    print("""usage: GITLAB_TOKEN="yourtoken" python get_releses.py""")
    exit()

GITLAB_TOKEN = os.environ["GITLAB_TOKEN"]


gl = gitlab.Gitlab('https://gitlab.com/', private_token=GITLAB_TOKEN)
gl.auth()
p = gl.projects.get("ryax-tech/dev/ryax-release")


print("# Release Notes")
print("")
print("These release notes provide information about new features, bug fixes and security updates that are included in Ryax.")
print("")
print("")


for r in p.releases.list(all=True):
    print("--------")
    print("## Ryax "+r.name)
    print("\nReleased the: "+r.released_at+"\n")
    txt = r.description
    txt = re.sub(r"^#### ", "##### ", txt, flags=re.MULTILINE)
    txt = re.sub(r"^### ", "#### ", txt, flags=re.MULTILINE)
    txt = re.sub(r"^## ", "### ", txt, flags=re.MULTILINE)
    txt = re.sub(r"^# ", "## ", txt, flags=re.MULTILINE)
    print(txt)
    print("")
