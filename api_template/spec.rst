
.. Ryax API documentation

.. _api-doc-__RYAX_API_VERSION__:

API version __RYAX_API_VERSION__
================================

.. raw:: html

    <link rel="stylesheet" type="text/css" href="../_static/swagger/swagger-ui.css" >
    <style>
      html
      {
        box-sizing: border-box;
        overflow: -moz-scrollbars-vertical;
        overflow-y: scroll;
      }
      *,
      *:before,
      *:after
      {
        box-sizing: inherit;
      }
      body
      {
        margin:0;
        background: #fafafa;
      }
    </style>
    <div id="swagger-ui"></div>
    <script src="../_static/swagger/swagger-ui-bundle.js"> </script>
    <script src="../_static/swagger/swagger-ui-standalone-preset.js"> </script>
    <script>
    window.onload = function() {
      // Begin Swagger UI call region
      const ui = SwaggerUIBundle({
        url: "../_static/api/__RYAX_API_VERSION__-spec.json",
        dom_id: '#swagger-ui',
        deepLinking: true,
        presets: [
          SwaggerUIBundle.presets.apis,
          SwaggerUIStandalonePreset
        ],
        plugins: [
          SwaggerUIBundle.plugins.DownloadUrl
        ],
        layout: "BaseLayout"
      })
      // End Swagger UI call region
      window.ui = ui
    }
    </script>
