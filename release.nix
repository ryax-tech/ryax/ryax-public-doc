{
  pkgs ? import (builtins.fetchTarball {
    # Descriptive name to make the store path easier to identify
    name = "nixos-21.05-2021-10-21";
    # Commit hash for nixos-unstable as of 2018-09-12
    url = "https://github.com/nixos/nixpkgs/archive/70904d4a9927a4d6e05c72c4aaac4370e05107f3.tar.gz";
    # Hash obtained using `nix-prefetch-url --unpack <url>`
    sha256 = "sha256:08vvir0npyrdx85ypiannwzvyryqdw3749bghffhdsq2dgz1cx8z";
  }) {}
}:

# Build sphinx HTML documentation.
pkgs.stdenv.mkDerivation rec {
  name = "ryax-user-html-documentation";

  src = ./.;

  buildInputs = with pkgs.python3Packages; [
    sphinx
    sphinx_rtd_theme
    recommonmark
  ];

  buildPhase = ''
    export SOURCE_DATE_EPOCH=$(date '+%s')
    make html
  '';
  installPhase = ''
    mkdir -p $out
    cp -r _build/html $out/
  '';
}

